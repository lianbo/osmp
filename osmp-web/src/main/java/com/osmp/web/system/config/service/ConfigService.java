package com.osmp.web.system.config.service;

public interface ConfigService {
    boolean refresh(String target);
}
